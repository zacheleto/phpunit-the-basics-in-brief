## PHPUnit the basics in brief
Covering the basics of PHPUnit where a BMI Calculator is tested based on the Body Mass Index Wikipedia Article [https://en.wikipedia.org/wiki/Body_mass_index] plus other tests that don't necessarily test the app but covers the most PHPUnit basics such as:

* common assertions
* common annotations
* dependencies tests
* testing errors and exceptions

## Author

* **Zache Abdelatif (Leto)** 

